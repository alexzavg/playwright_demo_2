import { test } from '../../fixtures/env_name';
import { expect } from '@playwright/test';
import { assertResponseArray } from '../../helpers/api.helper';

test.describe('Templates', () => {

  test('Check through array', async () => {

    await test.step('should check that response contains every value from the request', async () => {
      const request = {
        Phone: '15559871111',
        FirstName: 'FirstName',
        LastName: 'LastName',
        Address1: 'Address1',
        City: 'City',
        State: 'NY',
        Zipcode: '07764',
        Email: 'a@example.com',
        Form_Name: 'FormName',
        Campaign_Name: 'CampainName',
        CompanyHousehold: 'CompanyHousehold1',
        IBMLeadSource: 'IBMLeadSource1'
      };

      const responseBody = {
        Phone: '15559871111',
        FirstName: 'FirstName',
        LastName: 'LastName',
        Address1: 'Address1',
        City: 'City',
        State: 'NY',
        Zipcode: '07764',
        Email: 'a@example.com',
        Form_Name: 'FormName',
        Campaign_Name: 'CampainName',
        CompanyHousehold: 'CompanyHousehold1',
        IBMLeadSource: 'IBMLeadSource1'
      };

      await assertResponseArray(request, responseBody);
    });
    
  });

});