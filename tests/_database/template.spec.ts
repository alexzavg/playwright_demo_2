import { connectToPostgres, queryPostgres, disconnectFromPostgres } from './../../helpers/database.helper';
import { test } from '@playwright/test';

test('Test PostgreSQL connection', async () => {
  try {
    const client = await connectToPostgres();
    const rows = await queryPostgres(client, 'SELECT * FROM users');
    console.log(rows);
    await disconnectFromPostgres(client);
  } catch (error) {
    throw error;
  }
});
