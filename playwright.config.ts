import type { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  projects: [
    {
      name: 'template',
      testMatch: ['/tests/_templates/*.spec.ts']
    },
    {
      name: 'database',
      testMatch: ['/tests/_database/*.spec.ts']
    },
  ],
  timeout: 90000,
  expect: { 
    timeout: 90000 
  },
  globalSetup: 'utils/globalSetup.ts',
  reporter: [
    ['list']
  ],
  use: {
    actionTimeout: 90000,
    navigationTimeout: 90000,
    screenshot: 'off',
    video: {
      mode: 'off',
      size: {
        width: 1920,
        height: 1080
      }
    },
    trace: 'retain-on-failure',
    contextOptions: { // all context options https://playwright.dev/docs/api/class-browser
      recordVideo: {
        dir: './test-results/videos/',
        size: {
          width: 1920,
          height: 1080
        }
      },
      colorScheme: 'dark',
      serviceWorkers: 'allow'
    }
  }
};

export default config;