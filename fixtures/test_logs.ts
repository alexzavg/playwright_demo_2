// testInfo object https://playwright.dev/docs/api/class-testinfo
import { test as base } from '@playwright/test';
import { logger, color } from '../helpers/common.helper';

export const test = base.extend<{ saveLogs: void }>({

  saveLogs: [ async ({page}, use, testInfo) => {
    console.log(color.info(`<<< ENVIRONMENT: ${process.env.ENV_NAME} >>>`));
    await logger(page);
    await use();

    // Check test status
    if (testInfo.status !== testInfo.expectedStatus) {
      // do something if test FAILED
    }
  }, { auto: true } ]

});