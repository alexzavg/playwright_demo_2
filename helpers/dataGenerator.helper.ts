const createMobilePhoneNumber = require("random-mobile-numbers");

export class DataGenerator {

  // returns random mobile phone by country, e.g. 'US'
  async randomMobilePhone(country:string){
    let phone = createMobilePhoneNumber(country);
    return phone;
  }

  // Return random string with current date 
  async currentDateString(){
    var date = new Date();    
    var testData = date.getFullYear() + (date.getMonth() + 1) + date.getDate() + "_" + (date.getHours() + 3) + date.getMinutes() + date.getSeconds();
    return testData;
  }

  async randomEmail(length: number) {
    var string = '';
    var i;
    var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
    for (i = 0; i < length; i++) {
        string += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return ('autotest_' + string + '@gmail.com');
  };

  async randomString(length: number) {
    var str = '';
    var i;
    var characters = 'abcdefghijklmnopqrstuvwxyz';
    for (i = 0; i < length; i++) {
        str += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return str;
  };

  async randomNumber(length: number) {
    var str = '';
    var i;
    var numbers = '123456789';
    for (i = 0; i < length; i++) {
        str += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }
    let num = Number(str);
    return num;
  };

  async specialChar(length: number) {
    var char = '';
    var i;
    var specialCharacters = '!@#$^&*{}|_';
    for (i = 0; i < length; i++) {
        char += specialCharacters.charAt(Math.floor(Math.random() * specialCharacters.length));
    }
    return char;
  };

  // Get current time in ISO format, e.g: 2019-02-12T15:24:15.731Z
  async getCurrentTimeISO() {
    var date = new Date();
    date.setTime(Date.now());
    var time = date.toISOString();
    console.log(time);
    return time;
  };

};