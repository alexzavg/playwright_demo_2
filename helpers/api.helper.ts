/**
 * Documentation
 * https://playwright.dev/docs/api/class-apirequestcontext
 */
import { Page, expect } from "@playwright/test";
import { color } from "./common.helper";
let prettyjson = require('prettyjson');

export async function logRequest(data:any) {
  console.log(color.request(`\n<<<<<<<<<<<<<<<<< SENDING REQUEST <<<<<<<<<<<<<<<<<`));
  console.log(color.request(`\nRequest data: \n` + prettyjson.render(data)));
};

export async function logResponse(status:any, data:any) {
  console.log(color.response(`\n>>>>>>>>>>>>>>>>> RECEIVING RESPONSE >>>>>>>>>>>>>>>>>`));
  console.log(color.response(`\nResponse status code: ${status}`));
	console.log(color.response(`\nResponse body: ` + '\n' + prettyjson.render(data)));
};

export async function assert(assertionName:any, expectedData:any, actualData:any) {
  let EXP = await expectedData.toString();
  let ACT = await actualData.toString();
  expect(EXP).toEqual(ACT);
  console.log(color.success(`\n+++++++ ASSERTION PASSED [${assertionName}]: response data contains [${actualData}] & equals [${expectedData}] +++++++`));
};

export async function assertResponseArray(request:object, response:any) {
  try {
    // convert object to array
    let arr = Object.entries(request);
    console.log(request);

    // count array elements
    let primaryArrayLength = arr.length;
    console.log(`\nPrimary array length: ${primaryArrayLength}`);

    // check that response contains every "value" of the "key:value" pair in the array
    let i;
    for (i = 0; i <= (primaryArrayLength-1); i++) {
      console.log(color.info('\nArray element index:'), i);

      let value = arr[i][1];
      console.log(color.info('\nValue corresponding to array element index:'), value);

      //console.log(color.info('\nResponse body:'), '\n'+prettyjson.render(response));
      
      let responseString = JSON.stringify(response);
      
      expect(responseString).toContain(value);
      console.log(color.success(`+++++++ ASSERTION PASSED +++++++`));
    }
  } catch (error: any) {
    throw new Error(error);
  }
};

export async function getNetwokResponse(page: Page, URL: string, statusCode: number) {
  try {
    console.log(color.info(`Waiting for network request with URL: ${URL}`));

    const [resp] = await Promise.all([
      page.waitForResponse(r => 
        r.url().includes(URL)
      )
    ]);

    let status = await resp.status();
    let response = await resp.json();

    console.log(color.info(`\nRequest URL: ${URL} 
    \nResponse status code: ${status}
    \nResponse body: ` + '\n' + prettyjson.render(response)));

    await expect(status).toEqual(statusCode);

    return response;
  } catch (error: any) {
    throw new Error(error);
  }
};