import { Page } from "@playwright/test";
import * as zipsAndStates from '../fixtures/zip_by_state.json';
const chalk = require('chalk');

export const color = {
  success: chalk.bold.hex('#0EF15D'),
  error: chalk.bold.hex('#E4271B'),
  warning: chalk.bold.hex('#FFA500'),
  info: chalk.hex('#A020F0'),
  outgoingRequest: chalk.hex('#0560fc'),
  incomingRequest: chalk.hex('#fcf805'),
  request: chalk.hex('#0560fc'),
  response: chalk.hex('#fcf805')
};

export async function getRandomZipAndState(){
  let random = zipsAndStates.zipByState[Math.floor(Math.random() * zipsAndStates.zipByState.length)];
  let state = random.state;
  let zip = (Math.floor(Math.random() * (random.zip.max - random.zip.min) + random.zip.min)).toString();
  return {state, zip};
};

export async function logger(page: Page){
  page.on('request', request => 
    console.log(color.outgoingRequest('>>', request.method(), request.url()))
  );
  page.on('response', response =>
    console.log(color.incomingRequest('<<', response.status(), response.url()))
  );
  page.on('console', msg => {
    if(msg.type() == 'error'){
      console.log(color.error(msg.text));
    }
  });
};