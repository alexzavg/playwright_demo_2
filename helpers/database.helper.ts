import { Client } from 'pg';

export async function connectToPostgres() {
  // Connect to the PostgreSQL database
  const client = new Client({
    user: 'your_username',
    password: 'your_password',
    host: 'database_host',
    port: 5432,
    database: 'database_name'
  });
  await client.connect();
  return client;
}

export async function queryPostgres(client: Client, query: string) {
  // Query the database
  const res = await client.query(query);
  return res.rows;
}

export async function disconnectFromPostgres(client: Client) {
  // Disconnect from the database
  await client.end();
  console.log('Disconnected from PostgreSQL');
}
