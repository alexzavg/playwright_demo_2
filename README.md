- [Setup](#setup)

# Setup
To set the foundation for the tests on your local env simply go through the following steps:
1. Clone this repository into the root folder of Martech repository which you're working on
2. Install [nodejs & npm](https://nodejs.org/en/) 
3. Clone this repo
4. Run `npm i && npm i -g ts-node && npx playwright install --with-deps` in the project root folder
   - (optional) run `npm run dev` - if test runs, then you've done everything correctly